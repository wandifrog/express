var express = require("express");
var app = express();
var nunjucks = require("nunjucks");

app.use(express.static("public"));

nunjucks.configure("views", {
  autoescape: true,
  express: app
});
// app.set('views', './pages')

app.get("/", function(req, res) {
  res.render("index.njk");
});

app.get("/workspace2", function(req, res) {
  
  res.render("workspace2.njk");
});

app.listen(3000);
